#JS

## Datentypen

### Nichts
```js {cmd=node}
console.log(null);
console.log(undefined);
```

### Wahr oder Falsch (boolean)
```js {cmd=node}
let wahr = true;
if (wahr) {
    console.log("Es ist wahr");
}
```

### Zahlen

Es gibt nur einen Datentyp für Zahlen `double`.

```js {cmd=node}
console.log(1 + 2);
console.log(1 + 2.0);
console.log(1.1 + 2.1);
console.log(1 / 3);
console.log(Math.PI * 2);
console.log(Math.log2(32));
```

### Test (string)

Einfach oder doppelte Anfürhungszeichen markieren Text.

Empfehlung in JS Code einfach Anfürhungszeichen, im HTML doppelte verwenden.

```js {cmd=node}
console.log('Hello');
console.log("Hello");
console.log("Joe's Diner");
console.log('Er sagte: "GutenTag"');
console.log('She says, "let\'s go to joes\'s Diner"');
console.log('A\nB\nC\n');
```
Multiline Strings und String-Interpolationen mit einfachn Back-Ticks

```js {cmd=node}
console.log('Hello');
console.log(`Warum?
Wieso?
Weshalb`);

let name = 'Kurt'
console.log(`Hallo ${name}`)
```

### Variablen
```js {cmd=node}
let vorname = 'Marco'
let name = 'Schmalz'
let alter = '100'
let nervös = true
let now = new Date()
let jahrgang = now.getFullYear() - alter
if (nervös){
    console.log(`Herr ${name} ist nervös und im Jahr ${jahrgang} geboren`);
}
else{
    console.log(`Herr ${name} im Jahr ${jahrgang} geboren`);
}
```
Konstanten
```js {cmd=node}
const vorname = 'Marco'
// Kann danach nicht mehr bearbeitet werden
vorname = 'Märcu'
```

Die alte Version von `let` hiess `var`.

## Verschachtelte Datentypen

### Array
```js {cmd=node}
let zahlen = [2, 3, 5, 7, 11, 13]

console.log(zahlen[2]) // Dritte Primzahl

let auchZahlen = ['EINS', 'ZWEI', 'DREI']
console.log(auchZahlen[0])
console.log(`Anzahl Zahlen: ${auchZahlen.lenght}`)

let gemischt = [1, 'zwei', 3.1, 'vier']
console.log(gemischt[gemischt.length -1])

let komplexeZahlen = [[1, 1], [2, -1]]
let resultat = [
    komplexeZahlen[0][0] + komplexeZahlen[1][0],
    komplexeZahlen[0][1] + komplexeZahlen[1][1],
]
console.log(`${resultat[0]}` + `${resultat[1]}`)
```
Arrays können erweitert werden:
```js {cmd=node}
let teilnehmer = ['Märcu', 'Schmalz']
console.log(teilnehmer)

teilnehmer.push('Marco')
console.log(teilnehmer)

teilnehmer.pop()
console.log(teilnehmer)

ausfall = teilnehmer.pop()
console.log(teilnehmer)
console.log(`${ausfall} kann nicht teilnehmen`)
```

## Kontrollstrukturen

### if

```js {cmd=node}
let alter = 18

if (alter >= 18){
    console.log ('OK')
} else if (alter >= 16){
    console.log ('Vielleicht')
} else {
    console.log ('Nein')
}
```

Achtung bei Vergleichsoperatoren
`==` und `!=` nicht verwenden.

```js {cmd=node}
if (0 == ''){
    console.log('Hallo JS... GEITS BI DIR?????')
}


if (0 === ''){
    // Wird nicht ausgeführt
} else {
    console.log('So bisch es bravs!');
}
```
## for-loop

for mit `of` iteriert über den Inhalt.

```js {cmd=node}
let schule = 'TSBE'

for (let buchstabe of schule){
    console.log(buchstabe)
}
```

HP Rechner

```js {cmd=node}

let done = false

let stack = []
let input = [1, 2, 3, '*', '+']

for (let x of input){
    if (typeof x === 'number'){
        stack.push(x)
    } else if (x === "*"){
        let a = stack.pop()
        let b = stack.pop()
        stack.push(a * b)
    } else if (x === "+"){
        let a = stack.pop()
        let b = stack.pop()
        stack.push(a + b)
    } else {
        console.log('UPSSSSSS')
    }
}
console.log ('Resultat', stack[0])
```

mit `in` iterieren wir über die Indizes.

**Praxistipp:** immer `of` benutzen.

# while-loop

```js {cmd=node}
let done = false

let sum = 0
let zahlen = [1,3,5,7]

while (!done){
    if (zahlen.length === 0){
        done = true
    } else {
        sum += zahlen.pop()
    }
}
console.log(`Summe der Zahlen ${sum}`)
```

Colatz-Zahlen:
```js {cmd=node}
let zahl = Math.round(Math.random() * 500)

let done = false
while (!done){
    if (zahl % 2 === 0){
        zahl = zahl / 2
    } else {
        zahl = zahl * 3 + 1
    }
    console.log(zahl)
    if (zahl === 1) {
        done = true
    }

}
```

```js {cmd=node}
console.log('Hello');
```