# VoteNOW
VoteNOW ist eine Abstimmungsapp, welche es Freunden erleichtern soll gemeinsame Einsteidungen zu treffen. 

# Administrator
Der Administrator kann die Frage bearbeiten und selbst Antworten hinzufügen

# Benutzer
Der Benutzer kann die vom Administrator gestellte Frage beantworten

# Auswertung
Nach dem Abstimmen, erhält man eine Übersicht der Ergebnisse der gesamten Abstimmung
